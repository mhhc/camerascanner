package app.demo.camerascanner.ui.camera

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Size
import android.view.OrientationEventListener
import android.view.Surface
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.core.TorchState
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import app.demo.camerascanner.R
import app.demo.camerascanner.databinding.ActivityCameraScannerBinding
import app.demo.camerascanner.util.ImageAnalyzer
import com.google.common.util.concurrent.ListenableFuture
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class CameraScannerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCameraScannerBinding

    /**
     * TODO: read https://developer.android.com/topic/libraries/architecture/workmanager/advanced/listenableworker
     * a lightweight interface: it is a Future that provides functionality for attaching listeners and propagating exceptions.
     */
    private lateinit var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>

    /**
     * TODO: read https://developer.android.com/reference/java/util/concurrent/ExecutorService
     * An Executor that provides methods to manage termination and methods that can produce a Future
     * for tracking progress of one or more asynchronous tasks.
     * An ExecutorService can be shut down, which will cause it to reject new tasks
     */
    private lateinit var cameraExecutor: ExecutorService

    private var flashEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCameraScannerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cameraProviderFuture = ProcessCameraProvider.getInstance(this)

        // TODO: read https://developer.android.com/reference/java/util/concurrent/Executors#newSingleThreadExecutor(java.util.concurrent.ThreadFactory)
        cameraExecutor = Executors.newSingleThreadExecutor()

        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            bindPreview(cameraProvider)
        }, ContextCompat.getMainExecutor(this))

        // TODO: camera - overlay
//        binding.overlay.post {
//            binding.overlay.setViewFinder()
//        }
    }

    private fun bindPreview(cameraProvider: ProcessCameraProvider?) {
        if (isDestroyed || isFinishing) {
            return
        }
        cameraProvider?.unbindAll()

        val preview: Preview = Preview.Builder().build()
        val cameraSelector: CameraSelector = CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build()
        val imageAnalysis = ImageAnalysis.Builder()
                .setTargetResolution(Size(1280, 720))
                .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
                .build()
        val orientationEventListener = object : OrientationEventListener(this as Context) {
            override fun onOrientationChanged(orientation : Int) {
                val rotation : Int = when (orientation) {
                    in 45..134 -> Surface.ROTATION_270
                    in 135..224 -> Surface.ROTATION_180
                    in 225..314 -> Surface.ROTATION_90
                    else -> Surface.ROTATION_0
                }

                imageAnalysis.targetRotation = rotation
            }
        }
        orientationEventListener.enable()

        class ScanningListener : ImageAnalyzer.ScanningResultListener {
            override fun onScanned(result: String) {
                runOnUiThread {
                    imageAnalysis.clearAnalyzer()
                    cameraProvider?.unbindAll()
                    CameraScannerResultDialog.newInstance(
                            result,
                            object : CameraScannerResultDialog.DialogDismissListener {
                                override fun onDismiss() {
                                    bindPreview(cameraProvider)
                                }
                            })
                            .show(supportFragmentManager, CameraScannerResultDialog::class.java.simpleName)
                }
            }
        }

        var analyzer: ImageAnalysis.Analyzer = ImageAnalyzer(ScanningListener())
        imageAnalysis.setAnalyzer(cameraExecutor, analyzer)

        preview.setSurfaceProvider(binding.cameraScannerPreviewView.surfaceProvider)

        // Bind camera to lifecycle and setup flash
        val camera = cameraProvider?.bindToLifecycle(this, cameraSelector, imageAnalysis, preview)
        if (camera?.cameraInfo?.hasFlashUnit() == true) {
            binding.cameraScannerFlashIcon.visibility = View.VISIBLE
            binding.cameraScannerFlashIcon.setOnClickListener {
                camera.cameraControl.enableTorch(!flashEnabled)
            }
            camera.cameraInfo.torchState.observe(this) {
                it?.let { torchState ->
                    if (torchState == TorchState.ON) {
                        flashEnabled = true
                        binding.cameraScannerFlashIcon.setImageResource(R.drawable.ic_flash_on)
                    } else {
                        flashEnabled = false
                        binding.cameraScannerFlashIcon.setImageResource(R.drawable.ic_flash_off)
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    companion object {
        @JvmStatic
        fun getLaunchIntent(context: Context): Intent {
            return Intent(context, CameraScannerActivity::class.java)
        }
    }
}
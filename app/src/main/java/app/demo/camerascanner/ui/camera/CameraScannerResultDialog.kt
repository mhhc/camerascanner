package app.demo.camerascanner.ui.camera

import android.content.ActivityNotFoundException
import android.content.ClipData
import android.content.ClipboardManager
import android.content.DialogInterface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.URLUtil
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import app.demo.camerascanner.R
import app.demo.camerascanner.databinding.FragmentCameraScannerResultDialogBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

const val ARG_SCANNING_RESULT = "camera_scanning_result"

class CameraScannerResultDialog(private val listener: DialogDismissListener) : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentCameraScannerResultDialogBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCameraScannerResultDialogBinding.inflate(
            inflater, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val scannedResult = arguments?.getString(ARG_SCANNING_RESULT)
        binding.scannedResultInputText.setText(scannedResult)
        binding.scannedResultCopyButton.setOnClickListener {
            val clipboard = ContextCompat.getSystemService(requireContext(), ClipboardManager::class.java)
            val clip = ClipData.newPlainText(resources.getString(R.string.copied_message), scannedResult)
            clipboard?.setPrimaryClip(clip)
            Toast.makeText(requireContext(), resources.getString(R.string.copied_message), Toast.LENGTH_SHORT).show()
            dismissAllowingStateLoss()
        }
        binding.scannedResultBrowserButton.setOnClickListener {
            if (!scannedResult.isNullOrBlank()) {
                openCustomBrowser(scannedResult)
            } else {
                Toast.makeText(requireContext(), resources.getString(R.string.error_open_in_browser), Toast.LENGTH_SHORT).show()
            }
            dismissAllowingStateLoss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        listener.onDismiss()
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        listener.onDismiss()
    }

    interface DialogDismissListener {
        fun onDismiss()
    }

    private fun openCustomBrowser(result: String) {
        val url = if (URLUtil.isValidUrl(result)) {
            result
        } else {
            "https://www.google.com/search?q=${URLEncoder.encode(result, StandardCharsets.UTF_8.name())}"
        }
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        try {
            customTabsIntent.launchUrl(requireContext(), Uri.parse(url))
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(requireContext(), resources.getString(R.string.error_open_in_browser), Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        fun newInstance(scanningResult: String, listener: DialogDismissListener): CameraScannerResultDialog =
            CameraScannerResultDialog(listener).apply {
                arguments = Bundle().apply {
                    putString(ARG_SCANNING_RESULT, scanningResult)
                }
            }
    }

}
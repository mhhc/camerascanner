# Camera Scanner Demo App #
This is a demo app for self-learning on Android Jetpack and camera related library.

![App Icon](https://play-lh.googleusercontent.com/jZaU34dGjrZk0RK1-qq97i5To-LFMcwoxrMBtp2txHVi4CfdfJwBTvvziujrnUCXiVVb=w360-h155-rw)

![App Screenshot1](https://lh3.googleusercontent.com/DUI0JMVeCrz0VbbOeZfHgfyPfhS3Kt8v6BeKNHTbJ2WomlObvEj9vjTEBQrtJnL-N2-t)
![App Screenshot2](https://lh3.googleusercontent.com/xkyyRx63u-_wyRWb-Ugtb3jFpWxKxvUqe-21JRiGt6he6bQ4ic8gXJa6lFATNox_NA9n)

![App Screenshot3](https://lh3.googleusercontent.com/NDF_5uuOmpkz6A9R7Koo8U3BR3nk-rOd0ZUDCF1aavZMD0LFbMFhOlZ-pOv46nlIhg)
![App Screenshot4](https://lh3.googleusercontent.com/lIRxzkw1AaGd4_XbD6gTHBLE4JxuMsw6C-INV3LAPAuNi6KGmUh4gaD6Lps201GfAHE)

### Version History ###

#### Version 1.0.0 (1) - 2021.02.14 - It scans barcode & opens result in a CustomTab. ####
* Reverse engineered, referenced, and simplified Android open source projects to learn Android Jetpack & CameraX
    * https://github.com/maulikhirani/scannerX
    * https://github.com/googlesamples/mlkit/tree/master/android/vision-quickstart
    
* include Android Jetpack libraries
    * [integrate View Binding](https://developer.android.com/topic/libraries/view-binding)
        * "In most cases, view binding replaces findViewById."
    * [integrate CameraX](https://developer.android.com/training/camerax)
        * "The core CameraX libraries are in beta stage. Beta releases are functionally stable and have a feature-complete API surface. They are ready for production use but may contain bugs"
        * android.hardware.camera "This class was deprecated in API level 21."
        * android.hardware.camera2 vs CameraX 
            * ["Camera2 API is not deprecated; in fact, it is the foundation that CameraX is built on."](https://stackoverflow.com/questions/55803627/what-is-android-camerax)
            * [CameraX Status & Tested Devices](https://developer.android.com/jetpack/androidx/releases/camera)
    * include AndroidX Layouts / Material Design Components
        * BottomSheetDialogFragment https://material.io/develop/android/components/bottom-sheet-dialog-fragment
            * https://material.io/components/sheets-bottom#anatomy
        * ConstraintLayout https://developer.android.com/training/constraint-layout
        * CoordinatorLayout https://developer.android.com/reference/kotlin/androidx/coordinatorlayout/widget/CoordinatorLayout
        * MaterialCardView https://material.io/components/cards/android#card
        * TextInputLayout https://material.io/components/text-fields/android#using-text-fields
        * MaterialButton https://material.io/components/buttons/android#text-button
        * Dark theme https://developer.android.com/guide/topics/ui/look-and-feel/darktheme
        
* include MLKit Vision API
    * [integrate barcode scanning](https://developers.google.com/ml-kit/vision/barcode-scanning/android)
        * history: barcode scanning was in Mobile Vision API, then in Firebase API, now in ML Kit ondevice API
        * https://github.com/maulikhirani/scannerX 
        * https://github.com/googlesamples/mlkit/tree/master/android/vision-quickstart

* include Android Web: [Custom Tabs](https://developers.google.com/web/android/custom-tabs/implementation-guide) 
    * opens URL in an internal Chrome tab in the app (so it does not open the Chrome browser app)

* include [Open Source Notices](https://developers.google.com/android/guides/opensource)

* learn
    * [change Android package name](https://stackoverflow.com/questions/16804093/rename-package-in-android-studio)
    * create app icons with Image Asset Studio https://developer.android.com/studio/write/image-asset-studio
    * app signing with bundle and upload key https://developer.android.com/studio/publish/app-signing
        * https://support.google.com/googleplay/android-developer/answer/9859348?visit_id=637489542064173050-397060463&rd=1
            * "Starting August 2021, new apps will be required to publish with the Android App Bundle on Google Play."
    * private policy required for requesting camera permission https://stackoverflow.com/questions/42095522/privacy-policy-permission-s

### Future Learning ###

* [check out more ML Vision library](https://developers.google.com/ml-kit/guides)
    * [integrate with text recognition](https://developers.google.com/ml-kit/vision/text-recognition/android)
        * idea: scan a text and get a translation
* [check out ARCore](https://developers.google.com/ar/develop/java/quickstart)


### Reference & Credits ###

* https://developer.android.com/jetpack
* https://developer.android.com/training/camerax
* https://developers.google.com/ml-kit/guides
* https://github.com/maulikhirani/scannerX
* https://github.com/googlesamples/mlkit/tree/master/android/vision-quickstart
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)